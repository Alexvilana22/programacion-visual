﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InyenccionDependencias
{
    public class cocinero
    {

        IComida comida;

        public cocinero(IComida _comida)
        {
            this.comida = _comida;
        }
        
        public void Preparar()
        {
            this.comida.Preparar();
        }
    }
}
