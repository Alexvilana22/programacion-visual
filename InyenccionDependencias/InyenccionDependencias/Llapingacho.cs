﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InyenccionDependencias
{
    class Llapingacho : IComida
    {
        public string tipo { get; set; }
        public Llapingacho(string tipo)
        {
            this.tipo = tipo;
        }

        public void Preparar()
        {
            Console.WriteLine("Se prepara un Llapingacho " + tipo);
        }
    }
}